package gitlet;

import java.io.File;
import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/** Driver class for Gitlet, the tiny stupid version-control system.
 *  @author Iris Wu
 */
public class Main {
    /** Usage: java gitlet.Main ARGS, where ARGS contains
     *  <COMMAND> <OPERAND> .... */
    public static void main(String... args)
            throws IOException, ClassNotFoundException {
        _args = args;
        _length = args.length;
        String first = null;
        if (_length != 0) {
            first = _args[0];
        }
        Repo repo = deserialize(".gitlet/gitlet.ser");
        if (repo == null) {
            repo = new Repo();
        }
        if (_length == 0) {
            System.out.print("Please enter a command.");
            System.exit(0);
        } else if (!commandChecker(args)) {
            System.out.print("Incorrect operands.");
            System.exit(0);
        } else if (first.equals("init")) {
            repo.init();
        } else if (first.equals("add") && _length == 2) {
            repo.add(_args[1]);
        } else if (first.equals("commit")) {
            commitHelper(repo);
        } else if (first.equals("checkout")) {
            checkoutHelper(repo);
        } else if (first.equals("status") && _length == 1) {
            repo.status();
        } else if (first.equals("log") && _length == 1) {
            repo.log();
        } else if (first.equals("global-log") && _length == 1) {
            repo.globalLog();
        } else if (first.equals("rm") && _length == 2) {
            repo.remove(_args[1]);
        } else if (first.equals("find") && _length == 2) {
            repo.find(_args[1]);
        } else if (first.equals("branch") && _length == 2) {
            repo.branch(_args[1]);
        } else if (first.equals("rm-branch") && _length == 2) {
            repo.removeBranch(_args[1]);
        } else if (first.equals("merge") && _length == 2) {
            repo.merge(_args[1]);
        } else if (first.equals("reset")) {
            repo.reset(_args[1]);
        } else {
            System.out.print("No command with that name exists.");
            System.exit(0);
        }
        serialize(repo, ".gitlet/gitlet.ser");
    }

    /** Delegates action done by command "commit".
     * @param repo the current repository
     */
    public static void commitHelper(Repo repo) {
        if (_length == 1) {
            System.out.print("Please enter a commit message.");
            System.exit(0);
        } else if (_length == 2) {
            if (_args[1].equals("")) {
                System.out.print("Please enter a commit message.");
                System.exit(0);
            } else {
                repo.commit(_args[1]);
            }
        } else {
            System.out.print("Incorrect operands.");
            System.exit(0);
        }
    }

    /** delegates action done by check out.
     * @param repo the current repository.
     */
    public static void checkoutHelper(Repo repo) {
        if (_args.length == 2) {
            repo.checkoutBranch(_args);
        } else if (_args.length == 3) {
            if (_args[1].equals("--")) {
                repo.checkout(_args);
            } else {
                System.out.println("Incorrect operands.");
                System.exit(0);
            }
        } else if (_args.length == 4) {
            if (_args[2].equals("--")) {
                repo.checkoutID(_args);
            } else {
                System.out.print("Incorrect operands.");
                System.exit(0);
            }
        } else {
            System.out.print("Incorrect operands.");
            System.exit(0);
        }
    }

    /** Additional methods to help set up initial repository.
     * @param repo repository
     * @param filename the file names
     * @throws IOException
     * */
    public static void serialize(Repo repo, String filename)
            throws IOException {
        if (repo != null) {
            File file = new File(filename);
            FileOutputStream fileOut = new FileOutputStream(file);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(repo);
            out.close();
            fileOut.close();
        }
    }

    /** Additional methods to help set up initial repository.
     * @param filename the file names
     * @throws IOException
     * @return repo deserialized repo
     * */
    public static Repo deserialize(String filename)
            throws IOException, ClassNotFoundException {
        Repo repo = null;
        File file = new File(filename);
        if (file.exists()) {
            FileInputStream fileIn = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            repo = (Repo) in.readObject();
            in.close();
            fileIn.close();
        }
        return repo;
    }

    /** commandChecker for main.
     * @param args argument for main
     * @return whether the commands are valid or not
     */
    public static boolean commandChecker(String... args) {
        boolean find = false;
        for (String command: CMLIST) {
            if (command.equals(args[0])) {
                find = true;
            }
        }
        if (!find) {
            System.out.print("No command with that name exists.");
            System.exit(0);
        }
        if (args[0].equals("init") && _length == 1) {
            return true;
        } else if (args[0].equals("add") && _length == 2) {
            return true;
        } else if (args[0].equals("commit")) {
            if (_length == 1 | _length == 2) {
                return true;
            }
        } else if (args[0].equals("rm") && _length == 2) {
            return true;
        } else if (args[0].equals("log") && _length == 1) {
            return true;
        } else if (args[0].equals("global-log") && _length == 1) {
            return true;
        } else if (args[0].equals("find") && _length == 2) {
            return true;
        } else if (args[0].equals("status") && _length == 1) {
            return true;
        } else if (args[0].equals("checkout")) {
            if (_length == 2 | _length == 3 | _length == 4) {
                return true;
            }
        } else if (args[0].equals("branch") && _length == 2) {
            return true;
        } else if (args[0].equals("rm-branch") && _length == 2) {
            return true;
        } else if (args[0].equals("reset") && _length == 2) {
            return true;
        } else if (args[0].equals("merge") && _length == 2) {
            return true;
        }
        return false;
    }

    /** list of commands. */
    private static final String[] CMLIST = new String[] {
        "init", "add", "commit", "rm", "log",
        "global-log", "find", "status", "checkout",
        "branch", "rm-branch", "reset", "merge"};

    /** Input arguments by user. */
    private static String[] _args;

    /** length of the argument .*/
    private static int _length;

}
