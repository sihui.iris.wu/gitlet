package gitlet;

import java.io.Serializable;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map.Entry;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.HashMap;
import java.util.Random;
import java.util.LinkedHashMap;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Set;
import java.util.Map;
import java.util.ListIterator;

/**
 * Gitlet class.
 *
 * @author Iris Wu
 */
public class Repo implements Serializable {

    /** constructor for the repository. */
    public Repo() {
        _currBranch = null;
        _head = null;
        _branches = new HashMap<>();
        _allCommits = new LinkedHashMap<>();
        _remove = new HashSet<>();
        _staging = new HashSet<>();
        _tempRemove = new HashSet<>();
    }

    /** check whether the repository is initialized. */
    public void checkInit() {
        if (Files.exists(Paths.get(".gitlet"))) {
            System.out.println("A Gitlet version-control "
                    + "system already exists in the "
                    + "current directory.");
        }
    }

    /** check whether the repository is not initalized. */
    public void checkNotInit() {

    }

    /** initialize the repository. */
    public void init() throws IOException {
        checkInit();
        File gitletPath = new File(".gitlet");
        File stagePath = new File(_stagePath);
        File blobPath = new File(_blobPath);
        if (gitletPath.mkdir() && stagePath.mkdir() && blobPath.mkdir()) {
            _currBranch = "master";
            Commit commit = new Commit(null, "inital commit");
            _head = commit;
            if (_branches.get(_currBranch) == null) {
                LinkedList<Commit> commitList = new LinkedList<>();
                commitList.add(commit);
                _branches.put(_currBranch, commitList);
            } else {
                _branches.get(_currBranch).add(commit);
            }
            _allCommits.put(commit.getId(), commit);
        }
    }

    /** add method.
     * @param filename file name to be added */
    public void add(String filename) {
        String[] name = filename.split("/");
        String extracted = name[name.length - 1];
        if (!new File(filename).exists()) {
            System.out.println("File does not exist.");
            return;
        }

        if (_remove.contains(extracted)) {
            _remove.remove(extracted);
        }
        _staging.add(extracted);
        File prev = new File(filename);
        File curr = new File(_stagePath + extracted);
        String prevString = Utils.readContentsAsString(prev);
        if (_head.getBlobs().size() > 0) {
            for (String file : _head.getblobMap().keySet()) {
                String blobContent = _head.getblobMap().get(file);
                String currString = Utils.readContentsAsString(
                        new File(_blobPath + blobContent));
                if (prevString.equals(currString) && file.equals(filename)) {
                    curr.delete();
                    _staging.remove(extracted);
                }
            }
        }
        Utils.writeContents(curr, Utils.readContents(prev));
    }

    /** commit a certain commit id.
     * @param msg a commit ID.
     */
    @SuppressWarnings("unchecked")
    public void commit(String msg) {
        if (_staging.size() == 0 && _remove.size() == 0) {
            System.out.println("No changes added to the commit.");
            return;
        }
        Commit commit = new Commit(_head, msg);
        HashSet<String> tempStaging = (HashSet<String>) _staging.clone();
        for (String key : tempStaging) {
            commit.getTracking().add(key);
            File from = new File(_stagePath + key);
            String blobId = Utils.sha1(
                    Utils.readContentsAsString(from) + new Random().toString());
            File dest = new File(_blobPath + blobId);
            Utils.writeContents(dest, Utils.readContents(from));
            commit.getblobMap().put(key, blobId);
            _staging.remove(key);
            from.delete();
        }
        for (String key : _tempRemove) {
            commit.getTracking().remove(key);
            commit.getblobMap().remove(key);
        }
        _tempRemove.clear();
        for (String key : _remove) {
            commit.getblobMap().remove(key);
            _remove.remove(key);
        }
        commit.setId();
        if (_branches.get(_currBranch) == null) {
            LinkedList<Commit> newBranch = new LinkedList<>();
            newBranch.add(commit);
            _branches.put(_currBranch, newBranch);
        } else {
            _branches.get(_currBranch).add(commit);
        }
        _allCommits.put(commit.getId(), commit);
        _head = commit;
    }

    /** checkout a file.
     * @param args a given list of commits.
     */
    public void checkout(String[] args) {
        Commit commit = _head;
        if (!commit.getblobMap().containsKey(args[2])) {
            System.out.println("File does not exist in that commit.");
            return;
        }
        String id = commit.getblobMap().get(args[2]);
        byte[] b = Utils.readContents(new File(_blobPath + id));
        Utils.writeContents(new File(args[2]), b);
    }

    /** check out the ID of a commit.
     * @param args commit ID
     */
    public void checkoutID(String[] args) {
        String cid = args[1];
        Set<String> keys = _allCommits.keySet();
        Commit commit = null;
        for (String key : keys) {
            if (cid.equals(key.substring(0, cid.length()))) {
                commit = _allCommits.get(key);
                break;
            }
        }
        if (commit == null) {
            System.out.println("No commit with that id exists.");
            return;
        }
        if (!commit.getblobMap().containsKey(args[3])) {
            System.out.println("File does not exist in that commit.");
            return;
        }
        String id = commit.getblobMap().get(args[3]);
        byte[] content = Utils.readContents(new File(_blobPath + id));
        Utils.writeContents(new File(args[3]), content);
    }

    /** print out the log message for a commit, a helper method.
     * @param commit a commit
     */
    public void logCommit(Commit commit) {
        if (commit.getId().equals(_preventPrint)) {
            return;
        }
        System.out.println("===");
        System.out.format("commit %s\n", commit.getId());
        if (commit.getParent() != null
                && commit.getParent().size() == 2) {
            System.out.format(
                    "Merge: %s %s\n", commit.getParent().
                            get(0).getId().substring(0, 7),
                    commit.getParent().get(1).getId().substring(0, 7));
        }
        System.out.format("Date: %s\n", commit.getTime());
        System.out.println(commit.getlog());
        System.out.println();
    }

    /** general log method, calls on logCommit.
     */
    public void log() {
        Commit commit = _head;
        while (commit != null) {
            logCommit(commit);
            if (commit.getParent() != null) {
                commit = commit.getParent().getFirst();
            } else {
                commit = null;
            }
        }
    }

    /** detailing all the commits. */
    public void globalLog() {
        ListIterator<Map.Entry<String, Commit>> newSet = new ArrayList<>(
                _allCommits.entrySet()).listIterator(_allCommits.size());
        while (newSet.hasPrevious()) {
            Map.Entry<String, Commit> entry = newSet.previous();
            Commit commit = entry.getValue();
            logCommit(commit);
            System.out.println();
        }
    }

    /** prints out the status of the repository. */
    public void status() {
        if (_head == null) {
            System.out.println("Not in an initialized Gitlet directory.");
            System.exit(0);
        }
        TreeSet<String> set = new TreeSet<>();
        System.out.println("=== Branches ===");
        set.addAll(_branches.keySet());
        for (String curr: set) {
            if (curr.equals(_currBranch)) {
                System.out.print("*");
            }
            System.out.println(curr);
        }
        set.clear();
        System.out.println();
        System.out.println("=== Staged Files ===");
        set.addAll(_staging);
        for (String curr: set) {
            System.out.println(curr);
        }
        set.clear();
        System.out.println();
        System.out.println("=== Removed Files ===");
        set.addAll(_remove);
        for (String curr: set) {
            if (!new File(curr).exists()) {
                System.out.println(curr);
            }
        }
        set.clear();
        System.out.println();
        System.out.println("=== Modifications Not Staged For Commit ===");
        System.out.println();
        System.out.println("=== Untracked Files ===");
        System.out.println();
    }

    /** remove a file from the repository.
     * @param fileName file to be removed.
     * */
    public void remove(String fileName) {
        String[] name = fileName.split("/");
        String extracted = name[name.length - 1];
        _remove.add(extracted);
        if (!_staging.contains(extracted)
                && !_head.getTracking().contains(extracted)) {
            System.out.println("No reason to remove this file.");
            return;
        }

        if (_staging.contains(extracted)) {
            _staging.remove(extracted);
            new File(_stagePath + extracted).delete();
        }

        if (_head.getTracking().contains(extracted)) {
            _tempRemove.add(extracted);
            File file = new File(fileName);
            if (file.exists()) {
                file.delete();
            }
        }
    }

    /** find a commit with a given message.
     * @param message commit message
     */
    public void find(String message) {
        boolean find = false;
        for (Commit commit: _allCommits.values()) {
            if (commit.getlog().equals(message)) {
                System.out.println(commit.getId());
                find = true;
            }

        }
        if (!find) {
            System.out.println("Found no commit with that message.");
        }
    }

    /** add branch to the repository.
     * @param branch branch name
     */
    public void branch(String branch) {
        if (_branches.containsKey(branch)) {
            System.out.println("A branch with that name already exists.");
            return;
        }
        LinkedList<Commit> newBranchCommit = new LinkedList<>();
        Commit temp = _head;
        newBranchCommit.add(temp);
        while (temp.getParent() != null) {
            temp = temp.getParent().getFirst();
            newBranchCommit.addFirst(temp);
        }
        _branches.put(branch, newBranchCommit);

    }

    /** remove branch from the repository.
     * @param branch branchname
     */
    public void removeBranch(String branch) {
        if (!_branches.containsKey(branch)) {
            System.out.println("A branch with that name does not exist.");
        }
        if (_currBranch.equals(branch)) {
            System.out.println("Cannot remove the current branch.");
        }
        _branches.remove(branch);
    }

    /** check out branch from the repository.
     * @param args branchname
     */

    public void checkoutBranch(String[] args) {
        if (!_branches.keySet().contains(args[1])) {
            System.out.println("No such branch exists.");
            return;
        }
        if (args[1].equals(_currBranch)) {
            System.out.println("No need to checkout the current branch.");
            return;
        }
        Commit commit = _branches.get(args[1]).getLast();
        checkoutHelper(commit, 0);
        _currBranch = args[1];

    }

    /** reset a commit with a given ID.
     * @param commitID a given commit ID
     */
    public void reset(String commitID) {
        Commit curr = null;
        for (String cid: _allCommits.keySet()) {
            if (commitID.equals(cid.substring(0, commitID.length()))) {
                curr = _allCommits.get(cid);
                break;
            }
        }
        if (curr == null) {
            System.out.println("No commit with that id exists.");
            return;
        }
        _preventPrint = commitID;
        checkoutHelper(curr, 1);
        for (String branch: _branches.keySet()) {
            LinkedList<Commit> newcommits = _branches.get(branch);
            if (newcommits.contains(_head)) {
                _currBranch = branch;
                break;
            }
        }
    }

    /** delegates a certain portion of checkout.
     * @param commit commit to be checkouted
     * @param helper helpeer integer to determine whether
     *               additional moves need to be done
     */
    public void checkoutHelper(Commit commit, int helper) {
        for (String s : commit.getblobMap().keySet()) {
            if (!_head.getTracking().contains(s) && new File(s).exists()) {
                System.out.print("There is an untracked file in the way; "
                        + "delete it, or add and commit it first.");
                return;
            }
        }
        for (String uid : _head.getTracking()) {
            if (!commit.getblobMap().keySet().contains(uid)) {
                new File(uid).delete();
            }
        }

        for (String s : commit.getblobMap().keySet()) {
            String id = commit.getblobMap().get(s);
            byte[] content = Utils.readContents(new File(_blobPath + id));
            Utils.writeContents(new File(s), content);
        }
        if (helper == 0) {
            _head = commit;
        }
        if (helper == 1) {
            _staging.clear();
            _tempRemove.clear();
        }
    }

    /** check before merge.
     * @param givenBranch a branch to be merged
     */
    public void mergeCheck(String givenBranch) {
        if (!(_staging.isEmpty() && _remove.isEmpty())) {
            System.out.print("You have uncommited changes.");
            System.exit(0);
        }
        if (!_branches.containsKey(givenBranch)) {
            System.out.print("A branch with that name does not exist.");
            System.exit(0);
        }
        if (_currBranch.equals(givenBranch)) {
            System.out.print("Cannot merge a branch with itself.");
            System.exit(0);
        }
    }

    /** find the split point.
     * @param givenBranch given branch to find the split point
     * @return returns the split point commit
     */
    public Commit findSplit(String givenBranch) {
        LinkedList<Commit> target = _branches.get(givenBranch);
        LinkedList<Commit> curr = _branches.get(_currBranch);
        Commit result = null;
        for (Commit temp: target) {
            if (!curr.contains(temp)) {
                if (target.indexOf(temp) > 0) {
                    result = target.get(target.indexOf(temp) - 1);
                } else {
                    result = target.get(0);
                }
                break;
            }
        }
        return result;
    }

    /** check if ok to merge.
     * @param other other commit
     * @param split commit at the split point
     * @param current current commit
     * @param givenBranch branch to merge
     */
    public void checkMergeStatus(Commit other, Commit split,
                                 Commit current, String givenBranch) {
        if (split.getId().equals(other.getId())) {
            System.out.print("Given branch is an ancestor "
                    + "of the current branch.");
            System.exit(0);
        }
        if (split.getId().equals(_head.getId())) {
            checkoutBranch(new String[] {"checkout", givenBranch});
            System.out.print("Current branch fast-forwarded.");
            System.exit(0);
        }
        for (Entry<String, String> curr: other.getblobMap().entrySet()) {
            String fileInSplitId = split.getblobMap().get(curr.getKey());
            String fileInCurrentId = current.getblobMap().get(curr.getKey());
            if (fileInSplitId != null) {
                if (!curr.getValue().equals(fileInSplitId)) {
                    if (fileInCurrentId == null) {
                        if (new File(curr.getKey()).isFile()) {
                            System.out.print(
                                    "There is an untracked file in the way; "
                                    + "delete it, or add and commit it first.");
                            System.exit(0);
                        }
                    }
                }
            } else if (fileInCurrentId == null) {
                if (new File(curr.getKey()).isFile()) {
                    System.out.print("There is an untracked file in the way; "
                            + "delete it, or add and commit it first.");
                    System.exit(0);
                }
            }
        }
    }

    /** merge skeleton method.
     * @param givenBranch branch to merge
     */
    public void merge(String givenBranch) {
        mergeCheck(givenBranch);
        boolean conflict = false;
        Commit splitPoint = null;
        splitPoint = findSplit(givenBranch);
        Commit other = _branches.get(givenBranch).getLast();
        Commit current = _branches.get(_currBranch).getLast();
        checkMergeStatus(other, splitPoint, current, givenBranch);
        HashMap<String, String> sFiles =
                splitPoint.getblobMap();
        HashMap<String, String> target =
                _branches.get(givenBranch).getLast().getblobMap();
        HashMap<String, String> curr =
                _branches.get(_currBranch).getLast().getblobMap();

        conflict = checkConflict(givenBranch, target, curr, sFiles);
        for (String file : curr.keySet()) {
            if (!sFiles.containsKey(file) && !target.containsKey(file)) {
                if (file.equals("f.txt")) {
                    new File(file).delete();
                }
            }
        }

        Commit commit = new Commit(
                _branches.get(_currBranch).getLast(),
                _branches.get(givenBranch).getLast(),
                "Merged " + givenBranch + " into " + _currBranch + ".");
        mergeCommit(commit);

        if (conflict) {
            System.out.print("Encountered a merge conflict.");
        }
    }

    /** merge a commit.
     * @param commit a commit to merge
     * */
    @SuppressWarnings("unchecked")
    public void mergeCommit(Commit commit) {
        HashSet<String> tmpStaging = (HashSet<String>) _staging.clone();
        if (_staging.size() == 0 && _remove.size() == 0) {
            System.out.println("No changes added to the commit.");
            return;
        }
        for (String key : tmpStaging) {
            commit.getTracking().add(key);
            File from = new File(_stagePath + key);
            String blobId = Utils.sha1(
                    Utils.readContentsAsString(from)
                            + new Random().toString());
            File to = new File(_blobPath + blobId);
            Utils.writeContents(to, Utils.readContents(from));
            commit.getblobMap().put(key, blobId);
            _staging.remove(key);
            from.delete();
        }
        for (String key : _tempRemove) {
            commit.getTracking().remove(key);
            commit.getblobMap().remove(key);
        }
        _tempRemove.clear();
        for (String key : _remove) {
            commit.getblobMap().remove(key);
            _remove.remove(key);
        }
        commit.setId();
        if (_branches.get(_currBranch) == null) {
            LinkedList<Commit> com = new LinkedList<>();
            com.add(commit);
            _branches.put(_currBranch, com);
        } else {
            _branches.get(_currBranch).add(commit);
        }
        _allCommits.put(commit.getId(), commit);
        _head = commit;
    }

    /** check conflict status of a commit.
     * @param branch branch to consider
     * @param target target files
     * @param curr current files
     * @param splitFiles files at the split point
     * @return whether there is a merge conflict or not
     */
    private boolean checkConflict(String branch,
                                  HashMap<String, String> target,
                                  HashMap<String, String> curr,
                                  HashMap<String, String> splitFiles) {
        for (String targetFile : target.keySet()) {
            if (splitFiles.containsKey(targetFile)
                    && !target.get(targetFile).
                    equals(splitFiles.get(targetFile))
                    && !curr.containsKey(targetFile)) {
                mergeStage(targetFile, target);
            } else if (!splitFiles.containsKey(targetFile)
                    && !curr.containsKey(targetFile)) {
                _head = _branches.get(branch).getLast();
                checkout(new String[]{"checkout", "--", targetFile});
                _head = _branches.get(_currBranch).getLast();
                mergeStage(targetFile, target);
            } else if (!splitFiles.containsKey(targetFile)
                    && !curr.get(targetFile).
                    equals(target.get(targetFile))) {
                String currPath = _blobPath + curr.get(targetFile);
                String givenPath = _blobPath + target.get(targetFile);
                mergeContent(currPath, givenPath, targetFile);
                return true;
            }
        }
        for (String sFile : splitFiles.keySet()) {
            if (!target.containsKey(sFile)
                    && curr.containsKey(sFile)
                    && curr.get(sFile).equals(splitFiles.get(sFile))) {
                new File(sFile).delete();
            } else if (target.containsKey(sFile)
                    && curr.containsKey(sFile)
                    && !curr.get(sFile).equals(target.get(sFile))) {
                String currPath = _blobPath + curr.get(sFile);
                String givenPath = _blobPath + target.get(sFile);
                mergeContent(currPath, givenPath, sFile);
                return true;

            } else if (!target.containsKey(sFile)
                    && curr.containsKey(sFile)
                    && !curr.get(sFile).equals(splitFiles.get(sFile))) {
                String currContent = Utils.readContentsAsString
                        (new File(_blobPath + curr.get(sFile)));
                String givenContent = "";
                Utils.writeContents(new File(sFile),
                        "<<<<<<< HEAD\n" + currContent
                                + "=======\n" + givenContent + ">>>>>>>\n");
                return true;
            } else if (target.containsKey(sFile)
                    && !curr.containsKey(sFile)
                    && !target.get(sFile).equals(splitFiles.get(sFile))) {
                String currContent = "";
                String givenContent = Utils.readContentsAsString(
                        new File(_blobPath + target.get(sFile)));
                Utils.writeContents(new File(sFile),
                        "<<<<<<< HEAD\n" + currContent
                                + "=======\n" + givenContent + ">>>>>>>\n");
                return true;
            }
        }
        return false;
    }

    /** merge content of two different files.
     * @param currPath path of current files
     * @param targetPath path of target files
     * @param targetFile target file contents
     * @return merged file contents
     */
    public String mergeContent(String currPath,
                               String targetPath, String targetFile) {
        String s = null;
        String currContent = Utils.readContentsAsString(
                new File(currPath));
        String givenContent = Utils.readContentsAsString(
                new File(targetPath));
        s = "<<<<<<< HEAD\n" + currContent
                + "=======\n" + givenContent + ">>>>>>>\n";
        Utils.writeContents(new File(targetFile), s);
        return s;
    }

    /** merge then stage.
     * @param target target path
     * @param targetFile target files
     */
    public void mergeStage(String target, HashMap<String, String> targetFile) {
        String[] temp = target.split("/");
        String extracted = temp[temp.length - 1];

        if (_remove.contains(extracted)) {
            _remove.remove(extracted);
        }

        File old = new File(_blobPath + targetFile.get(target));
        File newFile = new File(_stagePath + extracted);
        String oldContent = Utils.readContentsAsString(old);
        byte[] oldCon = Utils.readContents(old);

        if (_head.getBlobs().size() != 0) {
            for (String blob: _head.getBlobs()) {
                File newFilePath = new File(_blobPath + blob);
                String newContent = Utils.readContentsAsString(newFilePath);
                if (oldContent.equals(newContent) && newFile.exists()) {
                    newFile.delete();
                    _staging.remove(extracted);
                }
            }
        }

        Utils.writeContents(newFile, oldCon);
        _staging.add(extracted);
        _head.getTracking().add(extracted);
    }


    /** current branch of the commit. */
    private String _currBranch;

    /** head pointer to a commit. */
    private Commit _head;

    /** hash map that logs all the branches and commits. */
    private HashMap<String, LinkedList<Commit>> _branches;

    /** all the commits found. */
    private LinkedHashMap<String, Commit> _allCommits;

    /** files to be removed. */
    private HashSet<String> _remove;

    /** staging area. */
    private HashSet<String> _staging;

    /** temp removal stage. */
    private HashSet<String> _tempRemove;

    /** unanimous blob path for the files. */
    private final String _blobPath = ".gitlet/blobs/";

    /** stage path fot eh files. */
    private final String _stagePath = ".gitlet/staging/";

    /** prevent printing. */
    private String _preventPrint;

}
