package gitlet;

import java.io.File;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.time.Instant;

/**
 * Commit Class.
 * @author Iris Wu
 */
public class Commit implements Serializable {

    /** blob path for all the blobs. */
    private final String _blobPath = ".gitlet/blobs/";

    /** sha1 id of the given commit. */
    private String _id;

    /** log of the given commit. */
    private String _log;

    /** time of the given commit. */
    private String _time;

    /** constructor for commit.
     * @param log commit log
     * @param parent  parent commit for the given commit.
     * */
    @SuppressWarnings("unchecked")
    public Commit(Commit parent, String log) {
        _id = getSHA1();
        _log = log;
        _time = new SimpleDateFormat("EEE MMM dd"
                + " HH:mm:ss yyyy Z").format(
                        new Date(Instant.EPOCH.getEpochSecond()));
        if (parent == null) {
            _log = "initial commit";
            _parent = null;
            _blobMap = new HashMap<>();
            _tracking = new HashSet<>();
        } else {
            _parent = new LinkedList<>();
            _parent.add(parent);
            _blobMap = (HashMap<String, String>) parent.getblobMap().clone();
            _tracking = (HashSet<String>) parent.getTracking().clone();
        }
    }

    /** another constructor for commit.
     * @param parent1 one parent for the commit
     * @param parent2 another parent for the commit
     * @param log log for the commit*/
    @SuppressWarnings("unchecked")
    public Commit(Commit parent1, Commit parent2, String log) {
        _log = log;
        _time = new SimpleDateFormat("EEE MMM dd"
                + " HH:mm:ss yyyy Z").format(
                        new Date(Instant.EPOCH.getEpochSecond()));
        _parent = new LinkedList<>();
        _parent.add(parent1);
        _parent.add(parent2);
        _blobMap = (HashMap<String, String>)
                parent1.getblobMap().clone();
        for (String key : parent2.getblobMap().keySet()) {
            _blobMap.put(key, parent2.getblobMap().get(key));
        }
        _tracking = (HashSet<String>) parent1.getTracking().clone();
        _tracking.addAll(parent2.getTracking());
        _id = getSHA1();
    }

    /** get the sha1 id for a commit commit. */
    public void setId() {
        _id = getSHA1();
    }

    /** return the id for a given commit.
     * @return _id
     * */
    public String getId() {
        return _id;
    }

    /** get the parent for a given commit.
     * @return _parent
     * */
    public LinkedList<Commit> getParent() {
        return _parent;
    }

    /** get the blob map for a given commit.
     * @return _blobMap
     * */
    public HashMap<String, String> getblobMap() {
        return _blobMap;
    }

    /** get the tracking blobs for the given commit.
     * @return _tracking
     * */
    public HashSet<String> getTracking() {
        return _tracking;
    }

    /** get the blobs (keyset) for the given commit.
     * @return set
     * */
    public HashSet<String> getBlobs() {
        HashSet<String> set = new HashSet<>();
        if (_blobMap != null) {
            for (String key : _blobMap.keySet()) {
                set.add(_blobMap.get(key));
            }
        }
        return set;
    }

    /** get sha1 id for a given commit.
     * @return _sha1
     * */
    public String getSHA1() {
        String hash = null;
        hash += "commit" + _time + _log + _parent;
        if (_blobMap != null) {
            for (String key : _blobMap.keySet()) {
                hash += Utils.readContentsAsString(
                        new File(_blobPath + _blobMap.get(key)));
            }
        }
        return Utils.sha1(hash);
    }

    /** get the log for a given commit.
     * @return _log
     * */
    public String getlog() {
        return _log;
    }

    /** return the time for a given commit.
     * @return _time
     * */
    public String getTime() {
        return _time;
    }

    /** parent commit for a given commit. */
    private LinkedList<Commit> _parent;

    /** tracking blobs. */
    private HashSet<String> _tracking;

    /** file name and blob id map for a given commit. */
    private HashMap<String, String> _blobMap;

}
