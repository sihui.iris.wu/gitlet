# Gitlet



## Project Description

A version control system that mimics the functionality of Git. In-class project project for CS61B, Fall 2021, taught by Professor Paul Hilfinger in U.C. Berkeley.

Complete project description can be found at https://inst.eecs.berkeley.edu/~cs61b/fa21/materials/proj/proj3/index.html
